package com.auto.APIFramework.repository;

import com.auto.APIFramework.model.ProductData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductData,Integer> {
}
