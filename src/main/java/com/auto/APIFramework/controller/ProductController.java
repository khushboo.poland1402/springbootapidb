package com.auto.APIFramework.controller;

import com.auto.APIFramework.model.Product;
import com.auto.APIFramework.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products")
    public List<Product> getProductList(){
        return productService.getAllProducts();
    }

//    @GetMapping("/products")
//    public List<Product> getProductByName(@RequestParam(value = "name") String name,@RequestParam(value = "fAssured") boolean fAssured){
//        return productService.getAllProducts().stream().filter(x->x.getName().equals(name)).collect(Collectors.toList());
//    }

    @GetMapping("/products/{id}")
    public Product getProduct(@PathVariable int id){
        return productService.getProductByID(id);
    }

    @PostMapping("/products")
    public List<Product> postProduct(@RequestBody Product product){
        return productService.AddProduct(product);
    }

    @PutMapping("/products/{id}")
    public Product updateProduct(@RequestBody Product product,@PathVariable int id){
        return productService.UpdateProduct(id,product);
    }

    @DeleteMapping("/products/{id}")
    public List<Product> deleteProduct(@PathVariable int id){
        return productService.DeleteProduct(id);
    }
}
