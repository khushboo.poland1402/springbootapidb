package com.auto.APIFramework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiFrameworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiFrameworkApplication.class, args);
		System.out.println("Springboot Application...");
	}

}
