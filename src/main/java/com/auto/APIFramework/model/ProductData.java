package com.auto.APIFramework.model;

import javax.persistence.*;

@Entity
@Table(name="Products")
public class ProductData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private String price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public ProductData() {}

    public ProductData(int id, String name, String price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String role) {
        this.price = role;
    }

    public void setId(int id) {
        this.id = id;
    }

}
