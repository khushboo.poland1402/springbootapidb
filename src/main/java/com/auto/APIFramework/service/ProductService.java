package com.auto.APIFramework.service;

import com.auto.APIFramework.model.Product;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    private List<Product> productList = new ArrayList<>(List.of(
            Product.builder()
                    .id(1)
                    .name("SAMSUNG Galaxy S10 (Prism White, 512 GB)")
                    .price(92000).build(),
            Product.builder()
                    .id(2)
                    .name("SAMSUNG Galaxy S24 Ultra 5G (Titanium Violet, 512 GB)")
                    .price(139999).build(),
            Product.builder()
                    .id(3)
                    .name("SAMSUNG Galaxy S24+ 5G (Cobalt Violet, 512 GB)")
                    .price(109999).build()
    ));

    public List<Product> getAllProducts() {
            return productList;
    }

    public Product getProductByID(int id) {
                return productList.stream()
                .filter(x->x.getId()==id)
                .findFirst().get();
    }

    public List<Product> AddProduct(Product product) {
        productList.add(product);
        return productList;
    }

    public Product UpdateProduct(int id, Product product) {
        return productList.stream()
                .filter(x->x.getId()==id)
                .peek(o->o.setName(product.getName()))
                .peek(o->o.setPrice(product.getPrice()))
                .findFirst().get();
    }

    public List<Product> DeleteProduct(int id) {
        productList.removeIf(x->x.getId()==id);
        return productList;
    }
}
