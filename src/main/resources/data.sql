--DROP TABLE IF EXISTS Products;

CREATE TABLE IF NOT EXISTS Products (id INTEGER PRIMARY KEY AUTO_INCREMENT, name VARCHAR(100) NOT NULL, price DOUBLE NOT NULL);
INSERT INTO Products (name,price) VALUES ('SAMSUNG Galaxy S10 (Prism White, 512 GB)',112000);
INSERT INTO Products (name,price) VALUES ('SAMSUNG Galaxy S24 Ultra 5G (Titanium Violet, 512 GB)',57000);
INSERT INTO Products (name,price) VALUES ('SAMSUNG Galaxy S24+ 5G (Cobalt Violet, 512 GB)',99999);

