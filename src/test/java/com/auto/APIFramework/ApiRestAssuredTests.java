package com.auto.APIFramework;

import com.auto.APIFramework.model.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiRestAssuredTests {

    @Value("${base.url}")
    private String baseURL;

    @LocalServerPort
    private int port;

    @Test
    public void testGetProductByID() {
        given()
                .baseUri(baseURL)
                .port(port)
                .basePath("/products/1")
                .get().then().assertThat().body("name", Matchers.equalTo("SAMSUNG Galaxy S10 (Prism White, 512 GB)"));

    }

    @Test
    void testGetProducts() {
        System.out.println("Test Post Product");

        //Arrange
        var product  = Product.builder()
                .id(1)
                .name("SAMSUNG Galaxy S10 (Prism White, 512 GB)")
                .price(92000).build();

        //Act
       var response = given().baseUri(baseURL)
               .port(port)
               .basePath("/products/1")
               .get();

       var responseEntity=response.body().as(Product.class);

        //Assert
        assertThat(responseEntity).isEqualTo(product);

    }

    @Test
    void testPostProducts() throws JsonProcessingException {
        System.out.println("Test Post Product	");

        //Arrange
        var product = Product.builder()
                .id(4)
                .name("SAMSUNG Galaxy S24 5G (Amber Yellow, 512 GB)")
                .price(89999).build();


        //Act
        var response = given().baseUri(baseURL)
                .port(port)
                .basePath("/products")
                .contentType("application/json")
                .body(product)
                .post();

        var responseEntity = response.body().as(Product[].class);

        var responseProduct = Arrays.stream(responseEntity)
                .filter(x -> x.getId() == 4)
                .findFirst().get();


        //Assert
        assertThat(responseProduct).isEqualTo(product);
    }

    @Test
    void testPutProducts() throws JsonProcessingException {
        System.out.println("Test Post Product");

        //Arrange
        var product = Product.builder()
                .id(1)
                .name("SAMSUNG Galaxy S10 (Prism White, 512 GB)")
                .price(92000).build();


        //Act
        var response = given().baseUri(baseURL)
                .port(port)
                .basePath("/products/1")
                .contentType("application/json")
                .body(product)
                .put();

        var responseEntity = response.body().as(Product.class);

        //Assert
        assertThat(responseEntity).isEqualTo(product);
    }
}
