package com.auto.APIFramework;

import com.auto.APIFramework.controller.ProductController;
import com.auto.APIFramework.model.Product;
import com.auto.APIFramework.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
public class ApiMockMvcTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Test
    public void testGetProductByID() throws Exception {
        //Arrange
        var product  = Product.builder()
                .id(1)
                .name("SAMSUNG Galaxy S10 (Prism White, 512 GB)")
                .price(92000).build();

        //Act
        when(productService.getProductByID(1)).thenReturn(product);

        //Assert
        this.mockMvc.perform(get("/products/1")).andExpect(status().isOk());
    }

    @Test
    public void testGetProducts() throws Exception {
        //Arrange
        var product  = Product.builder()
                .id(1)
                .name("SAMSUNG Galaxy S10 (Prism White, 512 GB)")
                .price(92000).build();

        //Act
        when(productService.getProductByID(1)).thenReturn(product);

        //Assert
        var result = this.mockMvc.perform(MockMvcRequestBuilders.get("/products/1"))
                .andExpect(MockMvcResultMatchers.jsonPath("name").value("SAMSUNG Galaxy S10 (Prism White, 512 GB)"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void testGetProductsList() throws Exception {
        //Arrange
        List<Product> productList = new ArrayList<>(List.of(
                Product.builder()
                        .id(1)
                        .name("SAMSUNG Galaxy S10 (Prism White, 512 GB)")
                        .price(92000).build(),
                Product.builder()
                        .id(2)
                        .name("SAMSUNG Galaxy S24 Ultra 5G (Titanium Violet, 512 GB)")
                        .price(139999).build(),
                Product.builder()
                        .id(3)
                        .name("SAMSUNG Galaxy S24+ 5G (Cobalt Violet, 512 GB)")
                        .price(109999).build()
        ));

        //Act
        when(productService.getAllProducts()).thenReturn(productList);

        //Assert
        var result = this.mockMvc.perform(MockMvcRequestBuilders.get("/products"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("SAMSUNG Galaxy S10 (Prism White, 512 GB)"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].price").value(92000))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
