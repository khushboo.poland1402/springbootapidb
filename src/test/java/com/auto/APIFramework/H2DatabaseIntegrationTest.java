package com.auto.APIFramework;

import org.json.JSONException;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class H2DatabaseIntegrationTest {

    @Test
    @Order(1)
    public void getAllProductsTest() throws JSONException {
        String expectedResult = "[\n" +
                "    {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"SAMSUNG Galaxy S10 (Prism White, 512 GB)\",\n" +
                "        \"price\": 92000.0\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 2,\n" +
                "        \"name\": \"SAMSUNG Galaxy S24 Ultra 5G (Titanium Violet, 512 GB)\",\n" +
                "        \"price\": 139999.0\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 3,\n" +
                "        \"name\": \"SAMSUNG Galaxy S24+ 5G (Cobalt Violet, 512 GB)\",\n" +
                "        \"price\": 109999.0\n" +
                "    }\n" +
                "]";

        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080/products", String.class);
        System.out.println("getAllProductsTest status code --> " + response.getStatusCode());
        System.out.println("getAllProductsTest response body --> " + response.getBody());

        JSONAssert.assertEquals(expectedResult, response.getBody(), false);
    }

    @Test
    @Order(2)
    public void getProductsByIDTest() throws JSONException {

        String expectedResult = "{\n" +
                "    \"id\": 2,\n" +
                "    \"name\": \"SAMSUNG Galaxy S24 Ultra 5G (Titanium Violet, 512 GB)\",\n" +
                "    \"price\": 139999.0\n" +
                "}";
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080/products/2", String.class);
        System.out.println("getProductsByIDTest status code --> " + response.getStatusCode());
        System.out.println("getProductsByIDTest response body --> " + response.getBody());

        JSONAssert.assertEquals(expectedResult, response.getBody(), false);
    }
}
//
//    @Test @Order(3)
//    public void getProductsByNameTest() throws JSONException {
//
//        String expectedResult = "{\n" +
//                "    \"id\": 1,\n" +
//                "    \"name\": \"SAMSUNG Galaxy S24+ 5G (Cobalt Violet, 512 GB)\",\n" +
//                "    \"price\": 109999,\n" +
//                "}";
//        TestRestTemplate restTemplate=new TestRestTemplate();
//        ResponseEntity<String> response= restTemplate.getForEntity("http://localhost:8080/products/name?name=SAMSUNG Galaxy S24+ 5G (Cobalt Violet, 512 GB)",String.class);
//        System.out.println("getProductsByNameTest status code --> "+ response.getStatusCode());
//        System.out.println("getProductsByNameTest response body --> "+ response.getBody());
//
//        JSONAssert.assertEquals(expectedResult,response.getBody(),false);
//    }
//
//}
