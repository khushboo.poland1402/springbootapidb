package com.auto.APIFramework;

import com.auto.APIFramework.model.Product;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.Arrays;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApiTestRestTemplate {

	@Autowired
	private TestRestTemplate testRestTemplate;

	@LocalServerPort
	private int port;

	@Test
	void testGetProductByID() {
		System.out.println("Test Get Product By ID");

		//Arrange
		final String baseURL = "http://localhost:" + port + "/products/1";
		var product  = com.auto.APIFramework.model.Product.builder()
				.id(1)
				.name("SAMSUNG Galaxy S10 (Prism White, 512 GB)")
				.price(92000).build();

		//Act
		var responseEntity=this.testRestTemplate.getForObject(baseURL, Product.class);

		//Assert
		assertThat(responseEntity).isEqualTo(product);
	}

	@Test
	void testGetProducts(){
		System.out.println("Test Get All Products");

		//Arrange
		final String baseURL = "http://localhost:" + port + "/products";
		var product  = com.auto.APIFramework.model.Product.builder()
				.id(1)
				.name("SAMSUNG Galaxy S10 (Prism White, 512 GB)")
				.price(92000).build();

		//Act
		var responseEntity=this.testRestTemplate.getForObject(baseURL, Product[].class);
		var responseProduct = Arrays.stream(responseEntity)
				.filter(x->x.getId()==1)
				.findFirst()
				.get();

		//Assert
		assertThat(responseProduct).isEqualTo(product);

	}

	@Test
	void testPostProducts() {
		System.out.println("Test Post Product	");

		//Arrange
		final String baseURL = "http://localhost:" + port + "/products";
		var product  = com.auto.APIFramework.model.Product.builder()
				.id(4)
				.name("SAMSUNG Galaxy S24 5G (Amber Yellow, 512 GB)")
				.price(89999).build();


		//Act
		var responseEntity = this.testRestTemplate.postForObject(baseURL,product, Product[].class);

		var responseProduct = Arrays.stream(responseEntity)
				.filter(x->x.getId()==4)
				.findFirst().get();


		//Assert
		assertThat(responseProduct).isEqualTo(product);

	}

}
